import { Flight } from "../components/flight-form/flight"

export class Mockflight {
  public static mflights: Flight[] = [
    {
      fullName: "Jhony Shin",
      from: "USA",
      to: "Bangkok",
      type: "One way",
      departure: new Date('2565-11-17'),
      arrival: new Date('2565-11-18'),
      adults: 1,
      children: 0,
      infants: 0,
    }
  ]
}
